.. _administration:

############################
Administration & Paramétrage
############################

Une section dédiée à l'administration et au pamétrage permet de configurer l'application : saisir les données des tables de références, gérer les utilisateurs, paramétrer les éditions PDF, ... Pour y accéder, un lien est disponible dans les actions 'raccourcis'.

.. figure:: a_administration_parametrage-action.png

    Raccourci 'Administration & Paramétrage'

Un menu permet de lister tous les écrans d'administration et paramétrage disponibles pour le profil de l'utilisateur connecté. Une recherche permet de filtrer les éléments disponibles dans ce menu.

.. figure:: a_administration_parametrage-menu.png

    Menu 'Administration & Paramétrage'

Dans ce paragraphe il est décrit les principaux éléments pour le manuel utilisateur, si un élément n'est pas décrit ici, il faut se référer au guide du développeur du framework openMairie :
https://docs.openmairie.org/?project=framework&version=4.10&format=html&path=usage/administration


.. contents::

*******
Général
*******

.. _administration__om_parametre:

Paramètres
**********

Ce paragraphe propose de lister tous les paramètres et leurs fonctions, qu'il est possible d'ajouter dans l'application.

Les champs à saisir dans om_parametre sont les suivant :

- **libelle** : le nom du paramètre
- **valeur** : la valeur que prend le paramètre


La liste des paramètres possibles dans l'application sont les suivants :

- **ville** : Paramètre permettant de spécifier la ville.
- **courrier_notification_objet** : Permet de paramétrer l'objet du mail envoyé lors de la notification par courriel d'un document issu d'un courrier.
- **courrier_notification_corps** :Permet de paramétrer le corps du mail envoyé lors de la notification par courriel d'un document issu d'un courrier. Doit obligatoirement contenir la variable de remplacement "[LIEN_TELECHARGEMENT_DOCUMENT]", qui sera remplacé lors de la génération du mail par le lien d'accès au document.
- **courrier_notification_url_acces** : Permet de paramétrer le lien d'accès au document du courrier de manière anonyme. Doit obligatoirement contenir la variable de remplacement "[CLE]" en paramètre de l'url 'key='. Exemple : http://localhost/opendebitdeboisson/web/notification.php?key=[CLE]
- **option_adresse_rivoli** : Permet de cacher/afficher les champs (ou colonne ou menu) faisant référence au code rivoli. Le code rivoli (ou rivoli ou encore parfois voie) est une donnée liée à une adresse. On retrouve cette référence si l'option est activé (valeur à true) dans les différents contextes suivants : 

  - Dans la section *géolocalisation* du menu *Administration & paramétrage*,
  - Dans le formulaire d'ajout d'un établissement,
  - Dans le formulaire d'ajout d'une adresse postale (également en colonne du listing),
  - Ou encore dans le formulaire d'ajout d'un périmètre d'exclusion. 

  Par défaut l'option *option_adresse_rivoli* est désactivé (valeur à *false*), ce qui cache toutes les références à ce champ. Si on passe la valeur de cette option à *true*, toutes les références seront à nouveau visibles et fonctionnelles.


******
Métier
******


.. _administration__contact_qualite:

Qualités de contact
*******************

Cette table de paramétrage permet de définir les qualités de contact. Ils sont utilisés dans le formulaire de :ref:`contact <etablissement__onglet_contacts>`.

De base ils peuvent être :

- exploitant
- propriétaire



.. _administration__declaration_type:

Types de déclaration
********************

Cette table de paramétrage permet de définir les types de déclaration. Ils sont utilisés dans le formulaire de :ref:`déclaration <declaration>`.

De base ils peuvent être :

• " D'OUVERTURE ".
• " DE TRANSLATION ".
• " DE MUTATION ".
• " DE TRANSFERT ".


.. _administration__nature_association:

Natures d'associations
**********************

Cette table de références permet de qualifier une :ref:`association <association>`.

Les valeurs peuvent être par exemple :

- Association sportive
- Association culturelle
- Autre



.. _administration__autorisation_decision:

Décisions d'autorisations
*************************

Cette table de paramétrage permet de définir les décisions d'autorisations exceptionnelles. Elles sont utilisées dans le formulaire de :ref:`autorisation_exceptionnelle <autorisation_exceptionnelle>`.

De base ils peuvent être :

• " Autorisation ".
• " Refus ".
• " Demande hors délai ".


.. _administration__type_licence:

Types de licences
*****************

Il s'agit des types de licence.

Ils sont utilisés dans le formulaire de licence et de déclaration.

De base ils peuvent être :

• " première catégorie ".
• " deuxième catégorie ".
• " troisième catégorie ".
• " quatrième catégorie ".
• " Licence restaurant ".
• " Licence à emportér ".
• " Petite licence à emporter ".
• " Petite licence restaurant ".

Il peuvent avoir ou non une contrainte de proximité (case à cocher).

Un champ panneau avec 4 paramètres est renseigné par défaut pour les types de licences suivants :

• licence de 3ème catégorie,
• licence de 4ème catégorie,
• licence restaurant,
• et petite licence restaurant.

Les 4 paramètres suivants peuvent être renseignés comme tel par exemple : 

• sigle = I, II, III ou IV,
• ruban = *LICENCE* ou *RESTAURANT*,
• décret = *Loi du 24 septembre 1941* ou *Décret du 8 février 1955*,
• thème = *LICENCE* ou *RESTAURANT*.

Les types de licences suivants peuvent être renseignés comme tel par exemple :
  - licence de 3ème catégorie : 
      - sigle=III 
      - ruban=LICENCE 
      - decret=Loi du 24 septembre 1941 
      - theme=licence
  - licence de 4ème catégorie : 
      - sigle=IV 
      - ruban=LICENCE 
      - decret=Loi du 24 septembre 1941 
      - theme=licence
  - licence restaurant : 
      - sigle=R 
      - ruban=RESTAURANT 
      - decret=Décret du 8 février 1955 
      - theme=restaurant
  - petite licence restaurant : 
      - sigle=PR 
      - ruban=RESTAURANT 
      - decret=Décret du 8 février 1955 
      - theme=restaurant

Une couleur peut être attribuée pour chaque type de licence, via le champ *couleur*, au format hexadécimal. (ex : a7e8b8)

Celle-ci sera présente dans les listings :
    - du menu latéral :
        - Établissement
        - Licence
        - Déclaration
    - des sous-formulaires :
        - Établissement -> déclarations
        - Licences -> établissements
    - des Types de licences du menu Administration & Paramétrage
    - du Widget des dernières déclarations

.. figure:: a_administration-type_licence-dashboard.png

    Dashboard

.. figure:: a_administration-type_licence-listing.png

    Menu 'Administration & Paramétrage'

.. _administration__derogation_decision:

Décisions de dérogation
***********************

Il s'agit des décisions de dérogation.

Elles sont utilisés dans le formulaire de DOT pour les avis du maire et les décisions du préfet.

Des champs spécifiques permettent de préciser :

- la **valorisation** en nombre de mois (permet de faire le calcul de la date de fin de validité de la DOT)
- l'**avis** correspondant : "Sans objet", "Favorable" et "Défavorable"

***************
Géolocalisation
***************


.. _administration__adresse_postale:

Adresses postales
*****************

Toutes les adresses de la commune.



.. _administration__rivoli:

Rivoli
******

Tous les codes RIVOLI de la commune.
Une option **option_adresse_rivoli** existe et permet d'afficher ou non les références au code Rivoli en fonction de sa valeur. Pour plus d'informations sur le paramétrage de l'option : :ref:`option_adresse_rivoli<administration__om_parametre>`.


.. _perimetre:

Les périmètres d'exclusions
***************************

Les périmètres d'exclusions sont de deux types :

- Le périmètre d'exclusion de type circulaire, calculé sur la base d’un ponctuel et d’un rayon. Ils indiquent à l’utilisateur la possibilité que l’élément concerné soit trop proche de l’établissement sur lequel porte une déclaration.
- Le périmètre d'exclusion de type surfacique (patatoïde), calculé sur la base d'un périmètre (ensemble de points formant un périmètre). Ils indiquent à l’utilisateur que l’établissement sur lequel porte une déclaration est en intersection avec un périmètre d'exclusion surfacique.

.. image:: a_administration-perimetre-listing.png

Les actions/fonctions disponibles depuis le listing sont :

- Action "ajouter"
- Action "consulter"
- Action "géolocaliser"
- Recherche simple
- Recherche avancée
- Export CSV


La recherche avancée
--------------------

.. image:: a_administration-perimetre-listing-recherche-avancee.png


La saisie des informations d'un périmètre d'exclusion (circulaire)
------------------------------------------------------------------

.. image:: a_administration-perimetre-formulaire-ajout-type-circulaire.png

Les champs sont les suivants :

- **Type de périmètre** (obligatoire) : Type de périmètre d'exclusion.
- **Libellé** (obligatoire) : Terme permettant d'identifier le périmètre d'exclusion circulaire.
- **Longueur exclusion** : Rayon du cercle en mètres autour du point. Au premier chargement du formulaire d'ajout, 
  le champ est pré-rempli avec la valeur ``150``.
- **Adresse** : En sélectionnant une adresse postale, on géolocalise le point représentant le centre du périmètre d'exclusion

.. image:: a_administration-perimetre-formulaire-adresse_postale.png

Le fonctionnement est identique à :ref:`la saisie de l'adresse postale d'un établissement<instruction_etablissement_formulaire_adresse_postale>`.


La saisie des informations d'un périmètre d'exclusion (surfacique)
------------------------------------------------------------------

.. image:: a_administration-perimetre-formulaire-ajout-type-surfacique.png

Les champs sont les suivants :

- **Type de périmètre** (obligatoire) : Type de périmètre d'exclusion.
- **Libellé** (obligatoire) : Terme permettant d'identifier le périmètre d'exclusion surfacique.

.. image:: a_administration-perimetre-formulaire-surfacique.png


********************
Tables de références
********************


.. _administration__titre_de_civilite:


Titres de civilité
******************

Cette table de références permet gérer les civilités des personnes.

Les valeurs peuvent être par exemple :

- Monsieur
- Madame



.. _administration__declaration_statut:

Statuts de la déclaration
*************************

Cette table de paramétrage permet de gérer l'avancement d'une :ref:`déclaration <declaration>`. Les statuts possibles de la déclaration sont les suivants :

• Le statut « En cours » indique une instruction en cours.
• Le statut « Validé » indique que la déclaration a fait l’objet d’une délivrance.
• Le statut « Refusé » indique que la déclaration a fait l’objet d’un refus de délivrance.
• Le statut « Expiré » indique que la déclaration a fait l’objet d’un accord puis a fait l’objet d’une nouvelle déclaration accordée. La nouvelle déclaration passe en statut « Validé » et l’ancienne déclaration passe en statut « Expiré ».


.. _administration__charte_type:

Types de chartes
****************

Cette table de références permet de qualifier une :ref:`charte_adhesion <charte_adhesion>`.

Les valeurs peuvent être par exemple :

- Charte pour la qualité de la vie nocture
- Charte d'engagement dispositif ANGELA
- Autre


.. _administration__charte_decision:

Types de décisions de chartes
*****************************

Cette table de références permet de qualifier une :ref:`charte_adhesion <charte_adhesion>`.

Les valeurs peuvent être par exemple :

- Favorable
- Favorable avec réserves
- Défavorable
- Autre


.. _administration__autorisation_etat:

États d'autorisations exceptionnelles
*************************************

Correspond aux états d'instruction des autorisations exceptionnelles.
Cette table de références permet de qualifier une :ref:`autorisation_exceptionnelle <autorisation_exceptionnelle>`.

Les valeurs peuvent être par exemple :

- En cours
- Validé
- Refusé
- Autre


.. _administration__autorisation_type:

Types d'autorisations exceptionnelles
*************************************

Cette table de références permet de qualifier une :ref:`autorisation_exceptionnelle <autorisation_exceptionnelle>`.

Les valeurs peuvent être par exemple :

- Bal
- DOTE
- Débit temporaire
- Autre


.. _administration__commission_type:

Types de commissions
********************

Cette table de références permet de qualifier une :ref:`commission <commission>`.

Les valeurs peuvent être par exemple :

- Commission de vie nocturne
- Commission d'arrondissement 1
- Autre


.. _administration__sanction_type:

Types de sanctions
******************

Cette table de références permet de qualifier une :ref:`sanction <sanction>`.

Les valeurs peuvent être par exemple :

- Avertissement
- Fermeture judiciaire
- Arrêté ERP
- Verbalisation de la Direction de la Santé
- Sanction individuelle (concernant l'exploitant)
- Autre


.. _administration__type_etablissement:

Types d'établissements
**********************

Cette table de références permet de qualifier un :ref:`établissement <etablissement>`.

Les valeurs peuvent être par exemple :

- Bar
- Discothèque
- Restaurant
- Salle municipale
- Autre



.. _administration__piece_type:

Types de pièces
***************

Cette table de références permet de qualifier une :ref:`pièce <etablissement__onglet_pieces>`.


Les valeurs peuvent être par exemple :

- Carte Nationalité d'Identité
- Passeport
- Cerfa
- Autre


.. _administration__service_consulte:

Services consultés
******************

Il s'agit des services consultés.
Ils sont utilisés dans le formulaire des :ref:`consultations <etablissement__onglet_consultations>` afin de désigner le service lié à la consultation ajoutée.

C'est le champ **libellé** qui est utilisé lors de la sélection du service consulté sur la consultation.

Les valeurs peuvent être pas exemple :

- Service ERP
- Mairie arrondissement
- Mairie de secteur
- Service des eau
- Service patrimoine

.. _administration__avis_consultation:

Avis des consultations
**********************

Il s'agit des avis des consultations.
Ils sont utilisés dans le formulaire des :ref:`consultations <etablissement__onglet_consultations>` afin de désigner quel avis a été rendu.

Les valeurs peuvent être pas exemple :

- Favorable
- Favorable avec réserves
- Défavorable
- Autre


********
Éditions
********
.. _administration__lettre_type:

Lettres types
*************

(:menuselection:`Administration & Paramétrage --> Éditions --> Lettre Type`)

Lettres types à définir avec ou sans contexte d'utilisation. Elles sont disponibles lors de la rédaction de courriers.


************************
Gestion Des Utilisateurs
************************

.. _administration__profil:


Les profils
***********

(:menuselection:`Administration & Paramétrage --> Gestion Des Utilisateurs --> Profil`)

Édition des profils présents dans l'application.

.. _administration__droit:


Les droits
**********

(:menuselection:`Administration & Paramétrage --> Gestion Des Utilisateurs --> Droit`)

Édition des droits présents dans l'application.


.. _administration__utilisateur:

Les utilisateurs
****************

(:menuselection:`Administration & Paramétrage --> Gestion Des Utilisateurs --> Utilisateur`)

Édition des utilisateurs présents dans l'application.


.. _administration__annuaire:

L'annuaire
**********

(:menuselection:`Administration & Paramétrage --> Gestion Des Utilisateurs --> Annuaire`)

Gestion des utilisateurs grâce à un annuaire LDAP ou ActiveDirectory.


****************
Tableaux de Bord
****************


.. _administration__widget:

Les widgets
***********

(:menuselection:`Administration & Paramétrage --> Tableaux De Bord --> Widget`)

Un widget, contraction de window (fenêtre) et gadget, est un composant du
tableau de bord proposant des informations.

Son paramétrage nécessite la saisie de quatre champs :

* **libellé** : le titre du widget
* **type** : *file* lorsqu'il s'agit d'un script ou *web* lorsqu'il s'agit d'un
  widget d'information
* **script** ou **lien** selon respectivement le type *file* ou *web* : nom du
  script ou URL du web service
* **arguments** ou **texte** selon respectivement le type *file* ou *web* :
  paramètres du script ou texte du widget (iframe, JavaScript, AJAX ...)

Seuls les widgets de type *file* sont utilisés dans openADS.

Les arguments sont déclarés ainsi :

::

  argument1=valeur1
  argument2=valeur2

Les scripts disponibles sont les suivants :

.. _administration__widget_dernieres_declarations:

dernieres_declarations
----------------------

Les informations fonctionnelles sont disponibles :ref:`ici<widget__dernieres_declarations>`. Aucun argument disponible sur ce widget.


.. _administration__widget_statistiques:

statistiques
------------

Les informations fonctionnelles sont disponibles :ref:`ici<widget__statistiques>`. Aucun argument disponible sur ce widget. 


.. _administration__widget_sanctions:

sanctions
---------

Les informations fonctionnelles sont disponibles :ref:`ici<widget__sanctions>`. Aucun argument disponible sur ce widget.

Quatre arguments facultatifs sont paramétrables :

* **type** [par défaut *aucune*] - Le type de la sanction
* **date_sanction** [par défaut *aucune*] - La date de la sanction
* **date_notification** [par défaut *aucun*] - La date de notification de la sanction
* **date_fin** [par défaut *aucun*] - La date de fin de sanction

Exemple d'une configuration pour obtenir le nombre de déclarations avec le statut *Validé* depuis le 01/01/2023 :

::

    type=type sanction 1
    date_fin=today + 15 jours

.. _administration__widget_declarations:

declarations
------------

Les informations fonctionnelles sont disponibles :ref:`ici<widget__declarations>`.

Trois arguments facultatifs sont paramétrables :

* **date_declaration** [par défaut *aucune*] - La date de début de la période sur laquelle on souhaite filtrer. Si l'argument n'est pas présent alors on ne filtre pas sur cet élément.
* **declaration_statut** [par défaut *aucun*] - les valeurs disponibles sont un des libellés de la table :ref:`statut de la déclaration <administration__declaration_statut>`. Si l'argument n'est pas présent alors on ne filtre pas sur cet élément.
* **declaration_type** [par défaut *aucun*] - les valeurs disponibles sont un des libellés de la table :ref:`type de déclaration <administration__declaration_type>`. Si l'argument n'est pas présent alors on ne filtre pas sur cet élément.

Exemple d'une configuration pour obtenir le nombre de déclarations avec le statut *Validé* depuis le 01/01/2023 :

::

    declaration_statut=Validé
    date_declaration=01/01/2023

.. _administration__widget_autorisations_exceptionnelles:

autorisations exceptionnelles
-----------------------------

Les informations fonctionnelles sont disponibles :ref:`ici<widget__autorisations_exceptionnelles>`.

5 arguments facultatifs sont paramétrables :

* **statut** [par défaut *aucun*]  Si l'argument n'est pas présent alors on ne filtre pas sur cet élément. Les valeurs disponibles sont un des libellés paramétrés dans la table :ref:`état des autorisations exceptionnelles <administration__autorisation_etat>`.
* **décision** [par défaut *aucun*] : permet de sélectionner les autorisations exceptionnelles selon la décision qu'ils ont reçu
* **date de décision** [par défaut *aucun*] : représente la date de décision minimale
* **date de début de validité de demande** [par défaut *aucun*] : représente la date de début de validité de la demande minimale
* **date de fin de validité de demande** [par défaut *aucun*] : représente la date de fin de validité de la demande minimale

::

    etat=En cours

.. _administration__composition:


Composition
***********

(:menuselection:`Administration & Paramétrage --> Tableaux De Bord --> Composition`)

Menu de composition du tableau de bord des utilisateurs.


