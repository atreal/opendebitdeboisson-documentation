.. _sanction:

***********
La sanction
***********


================================
Le listing général des sanctions
================================

(:menuselection:`Application --> Sanctions`)

.. image:: a_instruction-sanction-listing.png


La recherche avancée
====================


Elle peut porter sur les champs suivants : 

- **id sanction** : L'identifiant unique de la sanction
- **type de sanction** : Le type de la sanction
- **enseigne** : Le nom (libellé) de l'établissement sur lequel porte la sanction
- **n° de voie** : Le numéro de voie de l'établissement sur lequel porte la sanction
- **libellé de la voie** : Le nom de la voie de l'établissement sur lequel porte la sanction
- **code postal** : Le code postal de l'établissement sur lequel porte la sanction
- **type d'établissement**
- **décision** : La décision prise pour la sanction
- **statut** : Le statut (ou état) de la sanction
- **date de sanction** : La date de décision de la sanction
- **date de notificaation** : La date de début de la sanction
- **date de fin** : La date de fin de la sanction

.. image:: a_instruction-sanction-listing-recherche-avancee.png


L'export CSV
============

(:menuselection:`Application --> Sanctions`)

Depuis le listing des sanctions, il est possible d'en obtenir un export,
à l'aide du bouton CSV.

.. image:: a_instruction-sanction-listing.png


============================================================
Le listing des sanctions dans le contexte d'un établissement
============================================================

Depuis un établissement, l’onglet « sanctions » permet de gérer les sanctions rattachées à l’établissement.

.. image:: a_instruction-etablissement-onglet-sanction-listing.png


=========================================
La saisie des informations d'une sanction
=========================================

.. image:: a_instruction-etablissement-onglet-sanction-formulaire-ajout.png


Les champs sont les suivants : 

- **type de sanction** (obligatoire) :  :ref:`type de sanction <administration__sanction_type>`
- **date de sanction** (obligatoire)
- **date de notification**
- **date de fin** : (Permanente, Temporaire, Liée à un terrain de sport)
- **durée**
- **motif**
- **observations**



==========================
Alerte sur l'établissement
==========================

En cas d'une ou plusieurs sanctions sur un établissement, une alerte en indique le nombre sur la vue de l'établissement.

.. image:: a_instruction-sanction-alerte-etablissement-ajout.png

======================================
Widget paramétrable pour les sanctions
======================================

Le widget affichant le nombre de sanctions sur le tableau de bord peut être paramétré pour filtrer le nombre de résultat en fonction des arguments suivants :

- **type** : permet de sélectionner les sanctions par type
- **decision** : permet de sélectionner les sanctions selon la décision qui leur est attribuée (autorisation_decision)
- **date_sanction_min** : représente la date de sanction minimale
- **date_sanction_max** : représente la date de sanction maximale
- **date_notification_min** : représente la date de notification minimale
- **date_notification_max** : représente la date de notification maximale
- **date_fin_min** : représente la date de fin minimale
- **date_fin_max** : représente la date de fin maximale

Pour que les arguments de date soient pris en compte, il est obligatoire de commencer par le mot "today", représentant la date du jour. Il est ensuite possible d'ajouter ou soustraire des jours ou des mois à la date du jour, pour cela on ajoute à la suite "+ <nombre> days(jours) ou months(mois)" ou "- <nombre> days(jours) ou months(mois)".

Exemple de paramétrage pour afficher le nombre de sanctions ayant un état prédéfini et à J-4 avant la date limite de fin :

.. code::

    type=type sanction 1
    date_fin_min=today
    date_fin_max=today - 4 days

Autre exemple de paramétrage pour sélectionner les sanctions dont la date de fin de notification est avant le prochain mois :

.. code::

    date_notification_min=today
    date_notification_max=today + 1 month