.. _dot:

******
La DOT
******


==========================
Le listing général des DOT
==========================


.. image:: a_instruction-dot-listing.png


La recherche avancée
====================

.. image:: a_instruction-dot-listing-recherche-avancee.png


L'export CSV
============

Il est possible d'exporter le listing général des DOT en cliquant sur le bouton "CSV" disponible dans le coin supérieur droit du listing. Cet export prend en compte le filtrage effectué par la recherche avancée. 

====================================
La saisie des informations d'une DOT
====================================

L'ajout d'une DOT s'effectue dans l'onglet 'DOT' d'un établissement.

.. image:: a_instruction-dot-formulaire-ajout.png


Les champs sont les suivants : 

- **date de réception** 
- **date limite de réponse**
- **modalité de traitement** :  "Passage en commission" ou "Hors commission"
- **date avis du maire**
- **avis du maire**  : :ref:`décisions de dérogation <administration__derogation_decision>`
- **date envoi avis du maire**
- **date de décision du préfet**
- **décision du préfet** : :ref:`décisions de dérogation <administration__derogation_decision>`
- **date de fin de validité** : calculée à partir de la date de décision du préfet et la valorisation (en mois) de la décision du préfet.
- **commentaire**

Calcul de la date de fin de validité
====================================

À la validation du formulaire d'ajout ou de modification, si l'utilisateur saisit la date de réponse du préfet et ajoute une décision du préfet qui contient une valorisation (cf :ref:`décisions de dérogation <administration__derogation_decision>`), la date de fin de validité de la DOT est calculée automatiquement en fonction de ces deux champs.

================================
Widget paramétrable pour les DOT
================================

Le widget affichant le nombre de DOT sur le tableau de bord peut être paramétré pour filtrer le nombre de résultat en fonction des arguments suivants :

- **sans_avis** : permet de sélectionner (true) seulement les DOT sans avis ou avec avis (false)
- **date_reception_min** : représente la date réception minimale
- **date_reception_max** : représente la date réception maximale
- **date_limite_reponse_min** : représente la date limite de réponse minimale
- **date_limite_reponse_max** : représente la date limite de réponse maximale
- **date_fin_validite_min** : représente la date de fin de validité minimale
- **date_fin_validite_max** : représente la date de fin de validité maximale
- **date_avis_maire_min** : représente la date d'avis du maire minimale
- **date_avis_maire_max** : représente la date d'avis du maire maximale
- **date_decision_prefet_min** : représente la date de décision du préfet maximale
- **date_decision_prefet_max** : représente la date de décision du préfet maximale


Pour que les arguments de date soient pris en compte, il est obligatoire de commencer par le mot "today", représentant la date du jour. Il est ensuite possible d'ajouter ou soustraire des jours ou des mois à la date du jour, pour cela on ajoute à la suite "+ <nombre> days(jours) ou months(mois)" ou "- <nombre> days(jours) ou months(mois)".

Exemple de paramétrage pour afficher le nombre de DOT sans avis et à J-4 avant la date limite de réponse :

.. code::

    sans_avis=true
    date_limite_reponse_min=today
    date_limite_reponse_max=today + 4 days

Autre exemple de paramétrage pour sélectionner les DOT dont la date de fin de validité est dans 1 mois :

.. code::

    date_fin_validite_min=today
    date_fin_validite_max=today + 1 month
