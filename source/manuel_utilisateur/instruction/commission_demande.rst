.. _commission_demande:

***********************************
La demande de passage en commission
***********************************


========================================================
Le listing général des demandes de passage en commission
========================================================
(:menuselection:`Application --> Demandes de passage`)

Liste l'ensemble des demandes de passage en commission existantes.

.. image:: a_instruction-commission_demande-listing.png


La recherche avancée
====================

Elle peut porter sur les champs suivants : 

- **id commisson demande** : L'identifiant unique de la demande de passage en commission
- **enseigne** : L'établissement concerné par la demande de passage en commission
- **n° de voie** : Le numéro de voie du lieu de la demande de passage en commission
- **libellé de la voie** : Le libellé de voie du lieu de la demande de passage en commission
- **code postal** : Le code postal du lieu de la demande de passage en commission
- **date de commission** : La date de la demande de passage en commission
- **type** : Le type de la demande de passage en commission
- **avis** : L'avis rendu sur la demande de passage en commission

.. image:: a_instruction-commission-demande-listing-recherche-avancee.png


========================================================================
Le listing des demandes de passage en commission depuis un établissement
========================================================================
(:menuselection:`Application --> Établissements --> Commissions`)

C'est dans ce contexte, que l'ajout d'une demande de passage en commission se fait.

.. image:: a_instruction-commission_demande-listing_in_etablissement.png



======================================================================
Le listing des demandes de passage en commission depuis une commission
======================================================================
(:menuselection:`Application --> Commissions --> Demande de passage`)

C'est dans ce contexte, que l'ajout de l'avis sur une demande de passage en commission se fait.

.. image:: a_instruction-commission_demande-listing_in_commission.png


=================================================================
La saisie des informations d'une demande de passage en commission
=================================================================


.. image:: a_instruction-commission_demande-formulaire-ajout_in_etablissement.png

L'ajout d'une demande de passage en commission se fait exclusivement depuis le sous onglet **"commission"** depuis un établissement.

En fonction du contexte sélectionné (Charte, DOT ou Autre), un selecteur est proposé pour choisir la charte ou DOT concernée.
Si l'on choisit "Autre", aucun selecteur n'est ajouté.
Les champs **contexte** et **commission** Sont obligatoires.

Les champs sont les suivants : 


- **contexte** : *Charte*, *DOT* ou *Autre* (obligatoire)
- **sélectionner la charte** (selon le contexte choisi) : La charte concernée par la demande de passage
- **sélectionner la DOT** (selon le contexte choisi) : La déclaration d'ouverture tardive concernée par la demande de passage
- **commission** : La commission visée par la demande de passage (obligatoire)
- **observation** : Les observations éventuelles

