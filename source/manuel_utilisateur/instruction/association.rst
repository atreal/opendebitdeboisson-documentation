.. _association:

***************
L'association
***************


=====================================
Le listing général des associations
=====================================

.. image:: a_instruction-association-listing.png

Les actions/fonctions disponibles depuis le listing sont :

- Action "ajouter"
- Action "consulter"
- Recherche simple
- Recherche avancée
- Export CSV

La recherche avancée
====================

.. image:: a_instruction-association-listing-recherche-avancee.png

L'export CSV
============

Il est possible de faire un export au format CSV de l'ensemble du listing des associations.
Un bouton avec un petit logo sur lequel est marqué **csv** est visible en haut à droite du listing.

Les champs qui seront exportés sont les suivants :

- id
- nom de l'association
- numéro de création
- nature
- adresse

.. image:: a_instruction-association-listing.png


============================================
La saisie des informations d'une association
============================================

.. image:: a_instruction-association-formulaire-ajout.png

Les champs sont les suivants :

- **nom** (obligatoire) : ...
- **nature d'association** (obligatoire) : :ref:`nature d'association <administration__nature_association>` 
- **numéro de création**
- **numéro**
- **complément de numéro**
- **code RIVOLI**
- **libellé de la voie**
- **complément**
- **code postal**
- **ville**
- **date début validité**
- **date fin validité**


=======================================
Fiche de consultation d'une association
=======================================

.. image:: a_instruction-association-consultation.png

Depuis la fiche de consultation, il est possible de modifier ou supprimer l'association courante.
Il est également possible de visualiser les informations générales sur cette association.
 
S'il existe une autorisation exceptionnelle ayant comme "catégorie demandeur" l'association courante et qui possède une date de décision de début de validité, alors un tableau comptabilisant le(s) autorisation(s) exceptionnelle(s) liés à l'association sera affiché.
Il permet de visualiser rapidement le nombre d'autorisations exceptionnelles classées par année en lien avec l'association courante.

