.. _commission:

*************
La commission
*************


==================================
Le listing général des commissions
==================================
(:menuselection:`Application --> Commissions`)

Liste l'ensemble des commissions existantes.

.. image:: a_instruction-commission-listing.png


La recherche avancée
====================

Elle peut porter sur les champs suivants : 

- **id commisson** : L'identifiant unique de la commission
- **date de commission** : La date de séance de la commission
- **type de commission** : Le type de commission
- **état** : *en cours* ou *clôturé*

.. image:: a_instruction-commission-listing-recherche-avancee.png


L'export CSV
============
(:menuselection:`Application --> Commissions`)

Depuis le listing des commissions, il est possible d'en obtenir un export,
à l'aide du bouton CSV.

.. image:: a_instruction-commission-listing.png

===================================================
L'onglet 'demande de passage' depuis une commission
===================================================
(:menuselection:`Application --> Commissions --> Demande de passage`)

Depuis l'entrée de menu **commission**, l'onglet **demande de passage** permet de saisir l'avis sur une demande de passage en commission. Voir le détail dans le module :ref:`commission_demande <commission_demande>`.


===========================================
La saisie des informations d'une commission
===========================================


.. image:: a_instruction-commission-formulaire-ajout.png


Les champs sont les suivants : 


- **type de commission** :  :ref:`type de commission <administration__commission_type>` (obligatoire)
- **date de commission** : La date de séance de la commission
- **heure** : L'heure de séance de la commission (Champs de type *time*, ex : 09:53 ou 16:37)
- **état** : *en cours* ou *clôturé*
- **adresse** : L'adresse de la commission
- **complément** : Le complément d'adresse de la commission
- **salle** : La salle de séance de la commission
- **liste de diffusion** : La liste de diffusion des participants
- **participants** : La liste des participants
- **observation** : Les observations éventuelles

