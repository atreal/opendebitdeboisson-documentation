.. _etablissement:

***************
L'établissement
***************


=====================================
Le listing général des établissements
=====================================

.. image:: a_instruction-etablissement-listing.png

Les actions/fonctions disponibles depuis le listing sont :

- Action "ajouter"
- Action "consulter"
- Action "géolocaliser"
- Recherche simple
- Recherche avancée
- Export CSV

La recherche avancée
====================

.. image:: a_instruction-etablissement-listing-recherche-avancee.png

L'export CSV
============



=============================================
La saisie des informations d'un établissement
=============================================

.. image:: a_instruction-etablissement-formulaire-ajout.png

Les champs sont les suivants :

- **raison sociale** (obligatoire) : ...
- **enseigne** (obligatoire) : ...
- **SIRET**
- **forme juridique**
- **nature** (obligatoire) : permanent ou temporaire
- **type d'établissement** : :ref:`type d'établissement <administration__type_etablissement>` 
- **date de fermeture**
- **date de liquidation**
- **observations**


.. _instruction_etablissement_formulaire_adresse_postale:

La saisie de la localisation par l'adresse postale
==================================================

Depuis le formulaire d'ajout et de modification d'un établissement, il est possible de saisir automatiquement sa localisation en sélectionnant une adresse postale. Cela permet également de géolocaliser l'établissement.

.. image:: a_instruction-etablissement-formulaire-adresse_postale.png

Sur le formulaire d'ajout, par défaut les champs composants l'adresse sont bloqués, ils sont remplis automatiquement à la selection d'une adresse postale. Le champ permettant de sélectionner une adresse postale est un champ de recherche avec auto-complétion qui va cherche dans la table de référence des adresses postale.

Il est possible de passer en saisie manuelle en cochant la case "Activer la saisie manuelle".

Sur le formulaire de modification, la saisie manuelle est activée par défaut. Si celle-ci est désactivée une message demande de confirmer la suppression des valeurs des champs de l'adresse.
Sélectionner une nouvelle adresse postale changera également la géolocalisation de l'établissement.



La vue synthétique d'un établissement depuis une carte
======================================================

Depuis le widget, un clic sur "Accéder au lien" permet de visualiser la carte globale en plein écran.

.. image:: a_carte_globale.png

Il faut cliquer sur le point concerné et aller dans l'onglet Infos. 

.. image:: a_carte_globale_infos.png

Il est possible d'afficher une vue synthétique de l'établissement en cliquant sur sa raison sociale dans la liste des marqueurs.

.. image:: a_carte_globale_overlay.png


.. _etablissement_bloc_licence:

Bloc Licences
=============

.. image:: a_instruction-etablissement-bloc-licence.png

Lorsqu'une licence est marqué comme "Validé", les informations de cette licence sont affichés dans un bloc nommé **licences** sur la fiche de l'établissement.

Le bloc **licences** comporte les informations suivantes :

- **type de licence**
- **numéro de licence**
- **statut**
- **date de déclaration**
- **date de début d'exploitation**
- **DOT** : une indication sur la présence d'une DOT en cours de validité sur l'établissement. 


.. _etablissement__onglet_contacts:

===================
L'onglet 'contacts'
===================


Depuis un établissement, l'onglet "contacts" permet de gérer les contacts rattachés à l'établissement.

.. image:: a_instruction-contact-listing.png

Les actions/fonctions disponibles depuis le listing sont :

- Action “ajouter”
- Action “consulter”

La saisie des informations d'un contact
=======================================

.. image:: a_instruction-contact-formulaire-ajout.png

Les champs sont les suivants : 

- **type** : personne morale ou personne physique
- **raison sociale** (obligatoire si personne morale) : 
- **nom de naissance** (obligatoire si personne physique) : 
- **qualité de contact** : :ref:`qualité de contact <administration__contact_qualite>`
- **observations** : ...


.. _etablissement__onglet_declarations:

=======================
L'onglet 'déclarations'
=======================

Depuis un établissement, l'onglet "déclarations" permet de gérer les déclarations rattachées à l'établissement. Voir le détail dans le module :ref:`déclaration <declaration>`.


.. _etablissement__onglet_dot:

==============
L'onglet 'DOT'
==============

Depuis un établissement, l'onglet "DOT" permet de gérer les dérogations d'ouverture tardive rattachées à l'établissement. Voir le détail dans le module :ref:`DOT <dot>`.


.. _etablissement__onglet_autorisations_exceptionnelles:

========================================
L'onglet 'autorisations exceptionnelles'
========================================

Depuis un établissement, l'onglet "autorisations exceptionnelles" permet de gérer les autorisations exceptionnelles rattachées à l'établissement. Voir le détail dans le module :ref:`autorisation_exceptionnelle <autorisation_exceptionnelle>`.


.. _etablissement__onglet_charte_adhesion:

========================================
L'onglet 'chartes'
========================================

Depuis un établissement, l'onglet "chartes" permet de gérer les adhésions à des chartes rattachées à l'établissement. Voir le détail dans le module :ref:`charte_adhesion <charte_adhesion>`.


.. _etablissement__onglet_sanctions:

====================
L'onglet 'sanctions'
====================

Depuis un établissement, l'onglet "sanctions" permet de gérer les sanctions rattachées à l'établissement. Voir le détail dans le module :ref:`sanction <sanction>`.


.. _etablissement__onglet_commissions:

======================
L'onglet 'commissions'
======================

Depuis un établissement, l'onglet "commissions" permet de gérer les commissions rattachées à l'établissement. Voir le détail dans le module :ref:`commission <commission>`.


.. _etablissement__onglet_consultations:


========================
L'onglet 'consultations'
========================

Depuis un établissement, l'onglet "consultations" permet de gérer les consultations rattachées à l'établissement.

.. image:: a_instruction-consultation-listing.png

Les actions/fonctions disponibles depuis le listing sont :

- Action "ajouter"
- Action "consulter"

La saisie des informations d'une consultation
=============================================

.. image:: a_instruction-consultation-formulaire-ajout.png

En ajout d'une consultation les champs disponibles sont les suivants :

- **contexte** : permet de déterminer le contexte de la consultation ("Autorisation exceptionnelle", "DOT" ou "Autre")
- **autorisation exceptionnelle** : Si le champ **contexte** a pour valeur "Autorisation exceptionnelle" alors on peut sélectionner l'autorisation exceptionnelle correspondante. Voir le détail dans le module :ref:`autorisations exceptionnelles <autorisation_exceptionnelle>`.
- **DOT** : Si le champ **contexte** a pour valeur "DOT" alors on peut sélectionner la DOT correspondante. Voir le détail dans le module :ref:`DOT <dot>`.
- **service consulté** : :ref:`services consultés <administration__service_consulte>`
- **date de consultation** : la date de la consultation
- **date limite de réponse** : la date limite de réponse
- **commentaires**

L'action de génération du courrier de consultation
==================================================

.. image:: a_instruction-consultation-portlet-action-gen-courrier.png

Lorsque la consultation a été ajoutée, il est possible de générer le courrier de la consultation en cliquant sur l'action "*Générer le courrier de consultation*". Cette action va générer le courrier de consultation en fonction du contexte. Chaque contexte utilise un om_etat spécifique dont l'identifiant doit correspondre à une des valeurs suivantes :

- **consultation_dot**
- **consultation_auto_exceptionnelle**
- **consultation_autre**

Le fichier généré se retrouve dans le champ **courrier de consultation**, il est téléchargeable par l'utilisateur.

Lorsqu'on clique de nouveau sur l'action "*Générer le courrier de consultation*", le fichier enregistré se met à jour.

La saisie des informations concernant l'avis de la consultation
===============================================================

.. image:: a_instruction-consultation-formulaire-modification-bloc-reponse.png

La saisie de l'avis de consultation s'effectue dans le formulaire de modification de la consultation.

Le champs disponibles pour la saisie de l'avis sont les suivants :

- **date de réponse** : date de réponse de la consultation.
- **avis de la consultation** : :ref:`avis de consultation <administration__avis_consultation>`.
- **motivation** : champ de texte libre concernant la motivation de l'avis rendu.
- **courrier de réponse** : champ permettant d'ajouter un fichier correspondant au courrier de réponse du service.

.. _etablissement__onglet_pieces:

=================
L'onglet 'pièces'
=================

Depuis un établissement, l'onglet "pièces" permet de gérer les pièces (images ou pdf) rattachées à l'établissement.

.. image:: a_instruction-piece-listing.png

Les actions/fonctions disponibles depuis le listing sont :

- Action “ajouter”
- Action “consulter”
- Action "télécharger la pièce"



La saisie des informations d'une pièce
======================================

.. image:: a_instruction-piece-formulaire-ajout.png

Les champs sont les suivants : 

- **date de la pièce** (obligatoire) : ...
- **fichier** (obligatoire) : ...
- **type de la pièce** : :ref:`type de la pièce <administration__piece_type>`
- **observation** : ...


===================
L'onglet 'courrier'
===================

Depuis un établissement, l'onglet "Courriers" permet de gérer les courriers rattachés à l'établissement.

.. image:: a_instruction-courrier-listing.png

Les actions/fonctions disponibles depuis le listing sont :

- Action “ajouter”
- Action “consulter”
- Action "télécharger le courrier"


La saisie des informations d'un courrier
========================================

.. image:: a_instruction-courrier-formulaire-ajout.png

Les champs sont les suivants :

- **date du courrier** : La date du courrier
- **contexte** : permet de sélectionner le contexte du courrier (valeurs possibles : "déclaration", "DOT", "autorisation exceptionnelle", "sanction", "charte", "autre")
- **DOT** : ne s'affiche que si le contexte a pour valeur "DOT", permet de sélectionner une DOT liée à l'établissement.
- **autorisation exceptionnelle** : ne s'affiche que si le contexte a pour valeur "autorisation exceptionnelle", permet de sélectionner une autorisation exceptionnelle liée à l'établissement.
- **déclaration** : ne s'affiche que si le contexte a pour valeur "déclaration", permet de sélectionner une déclaration liée à l'établissement.
- **sanction** : ne s'affiche que si le contexte a pour valeur "sanction", permet de sélectionner une sanction liée à l'établissement.
- **charte** : ne s'affiche que si le contexte a pour valeur "charte", permet de sélectionner une charte liée à l'établissement.
- **modèle** (obligatoire) : :ref:`lettre type <administration__lettre_type>`. Pour chaque contexte la liste des modèles de lettre type change. Exemple : Lorsque mon contexte est "DOT", le champ select **modèle** n'affiche que les lettre type dont l'identifiant commence par "dot\_". Les préfixes d'identifiant de lettre type disponibles sont : "dot\_", "autorisation\_exceptionnelle\_", "declaraction\_", "sanction\_", "charte\_". Lorsqu'on sélectionne le contexte "autre", toutes les lettres type dont l'identifiant ne commence pas part un des éléments de la liste ci-dessus sont listés dans le champ select **modèle**.
- **objet** : L'objet du courrier
- **contacts** : permet de sélectionner un ou plusieurs contacts pour le courrier.


La modification des informations d'un courrier
==============================================

.. image:: a_instruction-courrier-formulaire-modification.png

La finalisation et la définalisation d'un courrier
==================================================

Le courrier à destinataire unique (mono)
****************************************

Lors de la finalisation d'un courrier, si celui-ci ne contient pas ou qu'un seul contact il est considéré comme "courrier mono", le document finalisé est généré et est disponible en utilisant l'action d'édition.

Après finalisation, un courrier *mono* reste modifiable afin de renseigner les dates de suivi.

.. image:: a_instruction-courrier-modification-mono-finalise.png

Lorsqu'on définalise un "courrier mono", le document enregistré est supprimé et est remplacé par un document contenant un filigrane afin d'indiquer qu'il est en cours d'édition lors du clique sur l'action "édition".


Le courrier à destinataires multiples (multi)
*********************************************

Si le courrier contient plusieurs contacts sélectionnés, alors il est considéré comme "courrier multi" ce qui entraîne, à la finalisation, l'ajout d'un courrier mono pour chaque contact enregistré dans le "courrier multi".

Il est possible d'identifier sur le formulaire du courrier multi la liste des "courriers mono" associés.

.. image:: a_instruction-list-courrier-enfants.png

Lors de la finalisation, un courrier *multi* n'est pas modifiable.

.. image:: a_instruction-courrier-enfants-listing.png

Lorsqu'on définalise un "courrier multi", tous les "courriers mono" associés sont supprimé et un document contenant un filigrane afin d'indiqué qu'il est en cours d'édition s'affiche lors du clique sur l'action "édition".


La notification par courriel
============================

.. image:: a_instruction-courrier-action-notification.png

Pour pouvoir procéder à la notification par courriel, il faut préalablement ajouter les paramètres courrier_notification_objet, courrier_notification_corps, courrier_notification_url_acces. Pour plus d'informations sur le paramétrage du mail rendez-vous dans :ref:`Liste des paramètres <administration__om_parametre>`.

Lorsqu'un "courrier mono" est finalisé, il est possible de notifier sont document par courriel à des adresses mail.

.. image:: a_instruction-courrier-overlay-notification.png

Pour cela il faut cliquer sur l'action "Notifier par courriel". Un overlay apparaît alors à l'écran avec deux champs :

- **contacts** : liste tous les contacts de l'établissement ayant une adresse mail
- **liste diffusion** : un champ texte libre permettant de lister les adresses mail pour lesquelles il faut notifier le document.

Si des contacts sont sélectionnés dans le champ **contacts**, lorsqu'on clique sur le bouton "Récupérer les adresses mail des contacts", les adresses mail des contacts sélectionnés sont listés dans le champ liste de diffusion séparé par des ";". Il est possible ensuite de saisir des adresses mail supplémentaires dans le champ liste de diffusion, à la suite.

Après avoir saisi les adresses mail dans la liste de diffusion, on clique sur le bouton "Valider" ce qui permet d'envoyer un mail contenant l'objet, le corps et le lien d'accès au document de manière anonyme paramétrés dans l'application à chaque contact.

.. image:: a_instruction-courrier-suivi-notification.png

Il est possible de retrouver dans la fiche du courrier un tableau de suivi de notification listant les notifications envoyé à chaque adresse mail et leur statut. Lors de la première consultation du fichier par le destinataire, la ligne de notification se met à jour pour afficher la date de premier accès, le statut "vu" et un commentaire "Le document a été vu".