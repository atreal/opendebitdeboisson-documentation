.. _declaration:

**************
La déclaration
**************

.. toctree::

===================================
Le listing général des déclarations
===================================

.. image:: a_instruction-declaration-listing.png


La recherche avancée
====================

.. image:: a_instruction-declaration-listing-recherche-avancee.png


L'export CSV
============

===============================================================
Le listing des déclarations dans le contexte d'un établissement
===============================================================


Depuis un établissement, l'onglet "déclarations" permet de gérer les déclarations rattachées à l'établissement.

.. image:: a_instruction-etablissement-onglet-declaration-listing.png

============================================
La saisie des informations d'une déclaration
============================================

.. image:: a_instruction-etablissement-onglet-declaration-formulaire-ajout.png

Les champs sont les suivants : 

- **date de déclaration** (obligatoire) : Au premier chargement du formulaire d'ajout, le champ est pré-rempli avec la date du jour.
- **date de précédente déclaration** : Au premier chargement du formulaire d'ajout, le champ est pré-rempli avec la plus grande date de début de validité de toutes les déclarations de l'établissement.
- **type de déclaration** (obligatoire) :  :ref:`type de déclaration <administration__declaration_type>` (d'ouverture/de transfert/de mutation)
- **contacts** : tous les contacts qui apparaissent sur la déclaration (exploitants, propriétaires, ...).
- **statut de la déclaration** :  :ref:`statut de la déclaration <administration__declaration_statut>` (en cours, validé, etc...)
- **type de licence** :  :ref:`type de licence <administration__type_licence>` (catégorie 1/2/3/4, de grande restauration, etc...)
- **n° de licence**
- **licence**
- **Date de début de validité de la déclaration** (obligatoire) : ...
- **Date de fin de validité de la déclaration** : ...


==============================================
Les actions disponibles depuis une déclaration
==============================================

.. image:: a_instruction-etablissement-onglet-declaration-permanent-formulaire-actions.png

Les actions disponibles depuis une déclaration :

- **CERFA de déclaration** : utilise l'état dont l'identifiant est *cerfa_declaration*
- **Récépissé** : utilise l'état dont l'identifiant est *recepisse*

.. image:: a_instruction-etablissement-onglet-declaration-temporaire-formulaire-actions.png



====================================================
Les possibilités de non-conformité d'une déclaration
====================================================


pour les établissements géolocalisés
====================================

Il y a 3 cas, pour chaque cas un message s'affiche :

• la distance entre la géolocalisation de l'établissement de la déclaration concernée et toutes les géolocalisations de périmètre interdit de type circulaire pour les demandes d'ouverture et de mutation permanente n'étant pas de première catégorie.

.. image:: a_instruction-etablissement-onglet-declaration-formulaire-message-perimetre-exclusion.png

• l'intersection entre la géolocalisation de l'établissement de la déclaration concernée et toutes les géolocalisations de périmètre interdit de type surfacique.

.. image:: a_instruction-etablissement-onglet-declaration-message-proximite-surfacique-etablissement.png

• la distance entre la géolocalisation de l'établissement de la déclaration concernée et toutes les géolocalisations des établissements possédant une licence de même type dont la période a au moins un jour de commun avec la période de la licence demandée.

.. image:: a_instruction-etablissement-onglet-declaration-formulaire-message-proximite-etablissement.png



pour tous les établissements (même non-géolocalisés)
====================================================

Il y a 1 cas pour chaque cas un message s'affiche :

• l'établissement doit disposer d'un permis d'exploitation.

.. image:: a_instruction-declaration-formulaire-message-permis.png
