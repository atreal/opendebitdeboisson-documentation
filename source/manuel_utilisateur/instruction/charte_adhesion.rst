.. _charte_adhesion:

*************************
La charte de vie nocturne
*************************


==============================================
Le listing général des adhésions à des chartes
==============================================

(:menuselection:`Application --> Chartes`)

.. image:: a_instruction-charte-adhesion-listing.png


La recherche avancée
====================

Elle peut porter sur les champs suivants : 

- **enseigne** : Le nom (libellé) de l'établissement sur lequel porte la charte
- **n° de voie** : Le numéro de voie de l'établissement sur lequel porte la charte
- **libellé de la voie** : Le nom de la voie de l'établissement sur lequel porte la charte
- **code postal** : Le code postal de l'établissement sur lequel porte la charte
- **décision prise** : Si la décision a été prise (Oui), si elle ne l'a pas été (Non) ou tous les résultats (Tous)
- **décision** : L'intitulé de la décision rendue
- **type de charte** : Le type de charte
- **date limite de réponse**
- **date de demande d'adhésion**
- **date de passage en commission**
- **date de décision**

.. image:: a_instruction-charte-adhesion-listing-recherche-avancee.png


L'export CSV
============

(:menuselection:`Application --> Chartes`)

Depuis le listing des chartes, il est possible d'en obtenir un export,
à l'aide du bouton CSV.

.. image:: a_instruction-charte-adhesion-listing.png
    

==========================================================================
Le listing des adhésions à des chartes dans le contexte d'un établissement
==========================================================================

Depuis un établissement, l’onglet « chartes » permet de gérer les chartes rattachées à l’établissement.

.. image:: a_instruction-etablissement-onglet-charte-adhesion-listing.png


=============================
Cartouche sur les commissions
=============================

Si des commissions sont en lien avec une charte,
elles apparaissent sous forme d'un cartouche sur la vue d'une charte d'un établissement.

.. image:: a_instruction-charte-adhesion-cartouches.png


===============================
Cartouche sur les consultations
===============================

Si des consultations sont en lien avec une charte, 
elles apparaissent sous forme d'un cartouche sur la vue d'une charte d'un établissement.

.. image:: a_instruction-charte-adhesion-cartouches.png


======================================================
La saisie des informations d'une charte d'autorisation
======================================================

Ajout d'une charte d'autorisation
=================================

.. image:: a_instruction-charte-adhesion-formulaire-ajout.png


Les champs sont les suivants : 

- **charte_type** (obligatoire) :  :ref:`type de charte <administration__charte_type>`
- **date_limite_adhesion**
- **date_demande_adhesion**
- **date_passage_commission**
- **date_decision_adhesion**
- **charte_decision** :  :ref:`décision de charte <administration__charte_decision>`
- **observations**
