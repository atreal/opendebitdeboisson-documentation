.. _licence:

**********
La licence
**********


===============================
Le listing général des licences
===============================


.. image:: a_instruction-licence-listing.png


La recherche avancée
====================

.. image:: a_instruction-licence-listing-recherche-avancee.png


L'export CSV
============

========================================
La saisie des informations d'une licence
========================================

.. image:: a_instruction-licence-formulaire-ajout.png


Les champs sont les suivants : 

- **n° de licence** (obligatoire)
- **type de licence** (obligatoire) : :ref:`type de licence <administration__type_licence>`
- **établissement actif** 
- **date d'ouverture**
- **date de fin**
- **saisie administrative** : Oui ou Non.
- **observations**

