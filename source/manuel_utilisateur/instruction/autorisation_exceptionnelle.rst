.. _autorisation_exceptionnelle:

*****************************
L'autorisation exceptionnelle
*****************************


====================================================
Le listing général des autorisations exceptionnelles
====================================================
(:menuselection:`Application --> Autorisations Exceptionnelles`)

.. image:: a_instruction-autorisation-exceptionnelle-listing.png


La recherche avancée
====================

Elle peut porter sur les champs suivants : 

- **type** : Le type d'autorisation exceptionnelle
- **type d'établissement**
- **décision** : La décision prise pour l'autorisation exceptionnelle
- **statut** : Le statut (ou état) de l'autorisation exceptionnelle
- **enseigne** : Le nom (libellé) de l'établissement sur lequel porte l'autorisation exceptionnelle
- **n° de voie** : Le numéro de voie de l'établissement sur lequel porte l'autorisation exceptionnelle
- **libellé de la voie** : Le nom de la voie de l'établissement sur lequel porte l'autorisation exceptionnelle
- **code postal** : Le code postal de l'établissement sur lequel porte l'autorisation exceptionnelle
- **date de décision** : La date de décision de l'autorisation exceptionnelle
- **date de début** : La date de début de l'autorisation exceptionnelle
- **date de fin** : La date de fin de l'autorisation exceptionnelle

.. image:: a_instruction-autorisation-exceptionnelle-listing-recherche-avancee.png


L'export CSV
============
(:menuselection:`Application --> Autorisations Exceptionnelles`)

Depuis le listing des autorisations exceptionnelles, il est possible d'en obtenir un export,
à l'aide du bouton CSV.

.. image:: a_instruction-autorisation-exceptionnelle-listing.png
    

================================================================================
Le listing des autorisations exceptionnelles dans le contexte d'un établissement
================================================================================

Depuis un établissement, l’onglet « autorisations exceptionnelles » permet de gérer les autorisations exceptionnelles rattachées à l’établissement.

.. image:: a_instruction-etablissement-onglet-autorisation-exceptionnelle-listing.png


============================================================
La saisie des informations d'une autorisation exceptionnelle
============================================================

La présence du champ association
================================

Par défaut lors de la création du contact, le type_demandeur sélectionné est l'*association*,    
le champ de saisie de l'association est alors disponible :

.. image:: a_instruction-autorisation-exceptionnelle-association-formulaire-ajout.png

Si la catégorie demandeur (type_demandeur) sélectionnée n'est pas une association (particulier ou professionnel), ce champ disparaît :

.. image:: a_instruction-autorisation-exceptionnelle-particulier-formulaire-ajout.png


La saisie des champs association et contact
===========================================

L'autocomplétion est présente sur les associations et les contacts.

Autocomplétion des associations :

.. image:: a_instruction-autorisation-exceptionnelle-autocompletion-association.png


Autocomplétion des contacts : 

.. image:: a_instruction-autorisation-exceptionnelle-autocompletion-contact.png

La création d'un nouvel enregistrement
======================================

Le click sur le lien *Créer un nouvel enregistrement* permet l'ouverture d'une fenêtre d'ajout,
pour les associations et les contacts.

.. image:: a_instruction-autorisation-exceptionnelle-autocompletion-association-nouveau.png


Les champs sont les suivants : 

- **autorisation_type** (obligatoire) :  :ref:`type d'autorisation <administration__autorisation_type>`
- **date_reception** (obligatoire)
- **autorisation_etat** :  :ref:`état d'autorisation <administration__autorisation_etat>`
- **type_demandeur** : (Association, Particulier, Professionnel)
- **association** : Présent si la catégorie demandeur (type_demandeur) est Association 
- **contact**
- **demande_motif**
- **demande_date_debut_validite**
- **demande_heure_debut** : Champs de type *time* (ex : 09:53 ou 16:37)
- **demande_date_fin_validite**
- **demande_heure_fin** : de type *time*
- **nb_personne_attendu**
- **autorisation_decision** :  :ref:`décision d'autorisation <administration__autorisation_decision>`
- **date_decision**
- **decision_date_debut_validite date**
- **decision_heure_debut** : de type *time*
- **decision_date_fin_validite**
- **decision_heure_fin** : de type *time*
- **commentaire**


=============================
Cartouche sur l'établissement
=============================

Si des autorisations exceptionnelles sont **présentes ou à venir** sur un établissement, 
elles apparaissent sous forme d'un cartouche sur la vue de l'établissement.

.. image:: a_instruction-autorisation-exceptionnelle-cartouche-etablissement.png

========================================
Alerte sur l'autorisation exceptionnelle
========================================

Lors de la validation de la fiche en modification ou en création, le nombre d’autorisations
exceptionnelles attribuées (pour l'association sélectionnée, sur l’année en cours) est calculé et affiché,
dans une alerte d’information de l’autorisation exceptionnelle.

.. image:: a_instruction-autorisation-exceptionnelle-alerte-etablissement.png


==========================================================
Widget paramétrable pour les autorisations exceptionnelles
==========================================================

Le widget affichant le nombre d'autorisations exceptionnelles sur le tableau de bord peut être paramétré pour filtrer le nombre de résultat en fonction des arguments suivants :

- **statut** : permet de sélectionner les autorisations exceptionnelles selon leur statut (ou état)
- **decision** : permet de sélectionner les autorisations exceptionnelles selon la décision qu'ils ont reçu
- **date_decision_min** : représente la date de décision minimale
- **date_decision_max** : représente la date de décision maximale
- **demande_date_debut_validite_min** : représente la date de début de validité de la demande minimale
- **demande_date_debut_validite_max** : représente la date de début de validité de la demande maximale
- **demande_date_fin_validite_min** : représente la date de fin de validité de la demande minimale
- **demande_date_fin_validite_max** : représente la date de fin de validité de la demande maximale


Pour que les arguments de date soient pris en compte, il est obligatoire de commencer par le mot "today", représentant la date du jour. Il est ensuite possible d'ajouter ou soustraire des jours ou des mois à la date du jour, pour cela on ajoute à la suite "+ <nombre> days(jours) ou months(mois)" ou "- <nombre> days(jours) ou months(mois)".

Exemple de paramétrage pour afficher le nombre d'autorisations exceptionnelles refusées pour les 4 prochains jours :

.. code::

    decision=Refus
    demande_date_debut_validite_max=today + 4 days

Autre exemple de paramétrage pour sélectionner les autorisations exceptionnelles dont la date de décision appartient au mois à venir :

.. code::

    date_decision_max=today + 1 month